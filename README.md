# 快速参考
- **维护者**：
  [Enmotech OpenSource Team](https://github.com/enmotech)

# 支持的tags
- [`latest`]()
- [`1.1.0`]()


# 关于Uqbar
Uqbar是在openGauss内核基础上开发的时序数据库，同时支持关系模型和时序模型，具备高性能、低成本、稳定可靠和开放易用等优点，是专为物联网场景设计的超融合时序数据库。Uqbar可以用于管理海量时序数据，也可以用于OLTP场景管理关系数据，还支持跨时序数据和关系数据的复杂关联查询，为物联网场景提供一站式数据解决方案。
MogDB官方网站：[https://mogdb.io/](https://mogdb.io/)

![logo](https://cdn-mogdb.enmotech.com/website/logo.png)


# 云和恩墨Uqbar镜像的特点
* 云和恩墨会最紧密跟踪Uqbar的源码变化，第一时间发布镜像的新版本。
* 云和恩墨的云端数据库，虚拟机数据库以及容器版本数据库均会使用同样的初始化最佳实践配置，这样当您在应对各种不同需求时会有几乎相同的体验。
* 云和恩墨会持续发布不同CPU架构（x86或者ARM）之上，不同操作系统的各种镜像

**目前已经支持x86-64和ARM64两种架构。**

从1.1.0版本开始
- Uqbar运行在[Ubuntu 20.04操作系统](https://ubuntu.com/)中

# 如何使用本镜像

## 启动Uqbar实例

```console
$ docker run --name uqbar --privileged=true -d -e GS_PASSWORD=Enmo@123 swr.cn-north-4.myhuaweicloud.com/uqbar/uqbar:latest
```

## 环境变量
为了更灵活的使用Uqbar镜像，可以设置额外的参数。未来我们会扩充更多的可控制参数，当前版本支持以下变量的设定。

### `GS_PASSWORD`
在你使用Uqbar镜像的时候，必须设置该参数。该参数值不能为空或者不定义。该参数设置了Uqbar数据库的超级用户omm以及测试用户uqbar的密码。Uqbar安装时默认会创建omm超级用户，该用户名暂时无法修改。测试用户uqbar是在[entrypoint.sh](https://github.com/enmotech/enmotech-docker-uqbar/blob/master/1.0.1/entrypoint.sh)中自定义创建的用户。

Uqbar镜像配置了本地信任机制，因此在容器内连接数据库无需密码，但是如果要从容器外部（其它主机或者其它容器）连接则必须要输入密码。

**Uqbar的密码有复杂度要求，需要：密码长度8个字符及以上，必须同时包含英文字母大小写，数字，以及特殊符号**

### `GS_NODENAME`

指定数据库节点名称 默认为uqbar

### `GS_USERNAME`

指定数据库连接用户名 默认为uqbar

### `GS_PORT`
指定数据库端口，默认为5432。

## 从容器外部连接容器数据库
Uqbar的默认监听启动在容器内的5432端口上，如果想要从容器外部访问数据库，则需要在`docker run`的时候指定`-p`参数。比如以下命令将允许使用15432端口访问容器数据库。
```console
$ docker run --name uqbar --privileged=true -d -e GS_PASSWORD=Secretpassword@123 -p 15432:5432 swr.cn-north-4.myhuaweicloud.com/uqbar/uqbar:latest
```
在上述命令正常启动容器数据库之后，可以通过外部的gsql进行数据库访问。
```console
$ gsql -d postgres -U uqbar -W'Secretpassword@123' -h your-host-ip -p15432
```


## 持久化存储数据
容器一旦被删除，容器内的所有数据和配置也均会丢失，而从镜像重新运行一个容器的话，则所有数据又都是呈现在初始化状态，因此对于数据库容器来说，为了防止因为容器的消亡或者损坏导致的数据丢失，需要进行持久化存储数据的操作。通过在`docker run`的时候指定`-v`参数来实现。比如以下命令将会指定将Uqbar的所有数据文件存储在宿主机的/enmotech/Uqbar下。`-u root`参数用于指定容器启动的时候以root用户执行脚本，否则会遇到没有权限创建数据文件目录的问题。

注：如果使用podman，会有目标路径检查，需要预先创建宿主机目标路径。

```console
$ mkdir -p /enmotech/Uqbar
$ docker run --name Uqbar --privileged=true -d -e GS_PASSWORD=Secretpassword@123 \
    -v /enmotech/Uqbar:/var/lib/Uqbar  -u root -p 15432:5432 \
    enmotech/Uqbar:latest
```

# License
Copyright (c) 2011-2023 Enmotech

许可证协议遵循GPL v3.0，你可以从下方获取协议的详细内容。

    https://github.com/enmotech/enmotech-docker-uqbar/blob/master/LICENSE
