# Setting SHELL to bash allows bash commands to be executed by recipes.
# This is a requirement for 'setup-envtest.sh' in the test target.
# Options are set to exit when a recipe line exits non-zero or a piped command fails.
SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

ARCH ?= $(shell uname -m)

$(warning $(ARCH))

ifeq ($(ARCH), i386)
    ARCH = i386
else ifeq ($(ARCH), i686)
    ARCH = 386
else ifeq ($(ARCH), x86_64)
    ARCH = amd64
else ifeq ($(ARCH), aarch64)
    ARCH = arm64
else
    $(error cpu arch not valid)
endif

IMAGE_PREFIX ?= docker.io
UQBAR_VERSION ?= 2.0.0

ifeq ($(UQBAR_VERSION), 2.0.0)
else
    $(error uqbar version $(UQBAR_VERSION) not valid, support 2.0.0)
endif

image-build: image-build-base image-build-uqbar-builder
	docker build \
        -f "Dockerfile" \
        -t "$(IMAGE_PREFIX)/uqbar:$(ARCH)-$(UQBAR_VERSION)" \
        --build-arg IMAGE_PREFIX=$(IMAGE_PREFIX) \
        --build-arg VERSION=$(UQBAR_VERSION) \
        --build-arg ARCH=$(ARCH) \
        .


image-build-base:
	docker build \
        -f "base/Dockerfile" \
        -t "$(IMAGE_PREFIX)/uqbar-base:$(ARCH)" \
        --build-arg ARCH=$(ARCH) \
        .

image-build-uqbar-builder:
	docker build \
        -f "uqbar/$(UQBAR_VERSION)/Dockerfile" \
        -t "$(IMAGE_PREFIX)/uqbar-builder:$(ARCH)-$(UQBAR_VERSION)" \
        --build-arg ARCH=$(ARCH) \
        .
