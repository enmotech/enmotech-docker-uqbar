ARG IMAGE_PREFIX
ARG VERSION
ARG ARCH

FROM ${IMAGE_PREFIX}/uqbar-builder:${ARCH}-${VERSION} as builder

FROM ${IMAGE_PREFIX}/uqbar-base:${ARCH}

ARG ARCH

COPY --from=builder /warehouse/gosu /usr/local/bin/gosu
COPY --chown=omm:omm --from=builder $GAUSSHOME $GAUSSHOME
COPY --chown=omm:omm --from=builder /warehouse/compat-tools /home/omm/compat-tools
COPY --chown=omm:omm --from=builder /warehouse/mogila-v1.0.0 /home/omm/mogila-v1.0.0

COPY entrypoint.sh /usr/local/bin/

RUN set -eux; \
    if [ "$ARCH" = "arm64" ];then \
        ln -s /lib/aarch64-linux-gnu/libreadline.so.8.0 /lib/aarch64-linux-gnu/libreadline.so.7; \
    elif [ "$ARCH" = "amd64" ];then \
        ln -s /lib/x86_64-linux-gnu/libreadline.so.8.0 /lib/x86_64-linux-gnu/libreadline.so.7; \
    fi && \
    chmod 755 /usr/local/bin/entrypoint.sh && \
    ln -s /usr/local/bin/entrypoint.sh /

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
EXPOSE 5432
CMD ["uqbar"]